# Course Project PDF 2022

## Assesment

### Design (10%)

Different design proposals available
Usage of one or more design techniques is visible from the introductory report
Sketches present different design options
Low-fidelty prototypes has been used and documented

### 2D and 3D Modeling and fabrication. (5%)

Use of appropiate 2D and 3D fabrication processes to meet their goals. Suboptimal processes need to be justified.
Students do 2D / 3D design on their own. Some parts might be taken from internet but not the whole project.
Use of different shapes, materials … is clearly justified
Discussion about different design options are provided.

### Electronics (5%)

Power issues are correctly deal with (use of right voltages, power sources …)
Project uses adequate electronic components to meet the functionality.
Circuits protection are correcly implemented (e.g. LED protective resistor, adquate use of MOSFET to power motors)

### Programming (5%)

Code is present and students made a significant contribution on it. Taking code from other sources is allowed, but students should adapt it and integrate with rest of code.
Correct usage of digital/analog inputs and outputs.
Program is working as expected. Multitask correctly implemented (different tasks running at the same time)
Simulations are provided in the documentation.
Advanced programming structure:
Networking
Interruptions
Complex Multitask

### Final prototype (45%)

### Protototype works as expected or has only some minor flaws (10%)

Protototype complexity. Minimum requirements: 1 sensor and 1 actuator (CANNOT PASS WITHOUT THAT) (5%)
If just basic requirements students do not get points here.
Number of functionalities that it implements
Number of additional sensors and actuators utilized.
Number of sensors/actuators used outside
Use of networking (communicate with other components / computer)

### Usability: (5%)

Protototype is easy to utilize: buttons accessible, on-switch, batteries easily to change.

### Integration (5%)

Mechanics and electronics integrated (e.g. non moving electronics)
Use of screws or at least tape to fix parts together (use of glue must be clearly justified)
Mechanical parts fits well with each other

### Aestetics(5%)

Project has a pleasant appearance. Details are visible. Not just a shoe-box

### Engineering(5%)

Project has used intelligent solution to cope with challenges appearing in design/fabrication phases.

### Creativity(5%)

Originality of the design/functionality

### Sustainability(5%)

Reusing components
Choosing most adequate and sustainable materials.
Building instead of buying
Easy to reuse different components of the project

### Documentation and presentations (20%)

Introductory report contains all required information (concept description, process, sketches, bill of materials)
The introductory report provides clear idea of what students are going to implement.
In general, weekly reports include all necessary parts (weekly goals, diary of process and mistakes)
Weekly reports are delivered on time. Just one missing weekly report accepted.
Weekly reports provide a good overview on the work made during that week.
There is a clear discussion on what happen, errors … Not only a description of the processes
Tasks of each participant and time is clearly specified.
Final report contains all required parts.
Final report contains information about sustainabilty.
The final project is summarized and pictures/videos provide a clear vision of the project
Reflection on the project results and lessons learn are clearly provided.
Final presentations summarizes correctly the project: main functionalities, different parts, processes utilized…

### Participation and group dinamics(10%)

Students attend and participate in mid and final presentation
Students attend and actively participate in tutoring times
Students met different courses deadlines
Members of the team has a clear role. Communication within the team is fluent.
Tasks are evenly distributed among team members.
