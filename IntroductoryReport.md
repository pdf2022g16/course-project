
We decided on a voice-activated locking mechanism. The idea is that whenever you give a voicecue loud enough, the lock would open. It would be 3D printed. We could also make a box from plywood that the lock could be augmented in. First we thought of a motion sensor that would just wave to your for example when you opened a door. In addition an automatic bottle opener was one of the ideas. Those ideas fell off quite fast because they were quite poor and useless. After that we ended up with the voice-activated lock and we stuck with that.

First idea picture:
blob:https://gitlab.com/30cf838f-1eb6-4222-b47d-eaf00f6e8436

Final idea pictures:
blob:https://gitlab.com/6855e648-92c0-4718-9cd6-e60f9af001e7
blob:https://gitlab.com/213aa172-c065-44fc-b651-adfa0d467fb3

# Bill of materials:
 -ARDUINO UNO
 -MICRO SERVO 9G (ACTUATOR)
 -MICROPHONE (SENSOR)
 -OTHER COMPONENTS INCLUDED IN THE CASE GIVEN OUT

# The sustainability of our design:

If we decide to make the box, then plywood is quite sustainable and we would have no problem with that because we wouldn't (hopefully) need any glue or other chemicals. 3D printing is also a very good option since we found out that, Polylactic acid (the printing material) is indeed biodegradeable under commercial composting conditions. In addition to that, the electronic parts used in the design will be reuseable if we were to disassemble it. In conclusion the design would be quite sustainable since all parts are reuseable and would not cause harm to the environment.
