#include <Servo.h>

///////////////////////////////////////////////
//Pins
#define SERVO_PIN 9
#define BUTTON_PIN 10
#define SOUND_SENSOR_PIN 11
#define INDICATOR_LED_PIN 12  
#define INDICATOR_LED_1_PIN 8 //Sound activation led

#define ANGLE_OPEN 10
#define ANGLE_CLOSED 170

#define SOUND_LIMIT 10
#define SOUND_SCALE 2
///////////////////////////////////////////////
Servo servo;

enum STATE_MASK{
  BUTTON_IS_DOWN = 1,
  BUTTON_WAS_DOWN = 2,
  SERVO_IS_OPEN = 4,
};

struct State{
  char mask;
  int soundCounter;
} state;
  
void setup(){
  //Serial
  //Serial.begin(9600);

  //Servo
  servo.attach(SERVO_PIN);
  servo.write(ANGLE_CLOSED);
  
  //Button
  pinMode(BUTTON_PIN, INPUT);

  //Indicator led
  pinMode(INDICATOR_LED_PIN, OUTPUT);
  pinMode(INDICATOR_LED_1_PIN, OUTPUT);

  //Sound sensor
  pinMode(SOUND_SENSOR_PIN, INPUT);

  //State
  memset(&state, 0, sizeof(state));
}
void loop(){
  //Set button bits
  state.mask = (state.mask & ~BUTTON_WAS_DOWN) | ((state.mask & BUTTON_IS_DOWN) << 1);
  state.mask = (state.mask & ~BUTTON_IS_DOWN) | (BUTTON_IS_DOWN * (digitalRead(BUTTON_PIN) == HIGH));

  //Update sound counter while button is down
  if(state.mask & BUTTON_IS_DOWN){
    if(!(state.mask & SERVO_IS_OPEN)){
      //Read sound sensor and set indicator led for sound 
      if(digitalRead(SOUND_SENSOR_PIN) == HIGH){
        digitalWrite(INDICATOR_LED_1_PIN, HIGH);
        delay(100);
        state.soundCounter += SOUND_SCALE;
      }else{  
        digitalWrite(INDICATOR_LED_1_PIN, LOW);
        delay(100);
      }
    }
  }else if(state.mask & BUTTON_WAS_DOWN){ //Button has been released
    digitalWrite(INDICATOR_LED_1_PIN, LOW);
    if(state.mask & SERVO_IS_OPEN){ //Close latch
      state.mask &= ~SERVO_IS_OPEN;
      servo.write(ANGLE_CLOSED);
    }else{
      if(state.soundCounter >= SOUND_LIMIT){ //Open latch if enough sound has been made
        //Open latch
        state.mask |= SERVO_IS_OPEN;
        servo.write(ANGLE_OPEN);
      }
    }
    state.soundCounter = 0;
  }

  //Update indicator led
  if(state.mask & SERVO_IS_OPEN){
    if(state.mask & BUTTON_WAS_DOWN){
      digitalWrite(INDICATOR_LED_PIN, HIGH);
    }else{
      digitalWrite(INDICATOR_LED_PIN, LOW);
    }
  }else{
    if(state.soundCounter >= SOUND_LIMIT){ //Indicate that latch can be opened
      digitalWrite(INDICATOR_LED_PIN, HIGH);
    }else{
      digitalWrite(INDICATOR_LED_PIN, LOW);
    }
  }
  
  delay(500);  
}
