[[_TOC_]]

# Week 1 [4-4-2022 - 10-4-2022]

## Goals

- Has the project idea been submitted to moodle?
- Setup gitlab page for the course's project.
- Maybe use gitlab issue page for tasks.
- Low fidelity prototype.
- Make sure that everyone has a basic understanding how to use git, and that everyone can pull and push to the repository.
- Discuss with the group how weekly report should be structured, common "cleanliness" of files.
- Group meetup on 7-4-2022, 12:00 at campus.

## Henrik Peteri

### Tasks
     
- Setting up gitlab page with necessary report files. [30 minutes]
- Trying to familiarize myself with Inkscape and Fusion 360 by making gear and gear rack. [2 hours]

### Reflection

- Since I'm familizar with git, it didn't take much time to set up repository for this project. Working as a team will be something new, which will be usefull practice.
- After some time I managed to find a option how to save files locally from fusion 360. Also by enabling offline working mode it no longer takes multiple minutes to startup.

## Tommi Väisänen
  
### Tasks

- Learn to use course software. [3 hours]

### Reflection

- Messed around with tinkercard/fusion360 to understand the basics.
   
# Week 2 [11-4-2022 - 17-4-2022]

## Goals

- Get something running on the arduino
- Box modeling
- Write missing introductory report

## Henrik Peteri

### Tasks

- Wrote servo rotation code. [1h]

### Reflection

- I wasn't expecting servo's rotation to be limited to 180 degrees. Luckily we hadn't cut any gears yet, This would have changed diameter of the locking mechanism gear. Since we abandoned the gear system shortly after and swapped it for a latch, rotation limit isn't a problem any more.

## Tommi Väisänen

- Looked more into how arduino works and learned tinkercad.

## Group Reflection

- Since our first idea was a lock for a door, but which was later changed to a box, we have updated our design to not use gears but a latch instead.
- Our Arduino seems to be broken. After trying to connect it to multiple computers using different cables, it just won't stay connected. One part of the board gets really hot and the board disconnects and the power led turns off aswell.
- We used Boxes.py for our first box desigs, which was fairly easy to use.

## 13-4-2022 Group Tutoring
  
- Attended at fablab. We were instructed to create a box and get something physically functional, which should have been done already at this point in the course.

# Week 3 [18-4-2022 - 24-4-2022]
  
## Goals
  
- Cut box for components, maybe try cardboard first.
- Get locking mechanism working.
- Find a microphone / sound sensor somewhere (SP Elektroniikka has a sound sensor module for 2e [ https://www.spelektroniikka.fi/p24093-sound-sensor-aani-ilmaisin-analoginen-ja-digitaalinen-lahto-vaikkapa-arduino-sov-fi.html ]).
  
## Henrik Peteri

### Tasks

- Swapped broken Arduino uno for a Arduino nano [10 min]
- Trying to get servos to work without any luck [5h]
- Update weekly report [2h]
- Writing code for button state tracking and looking into interrupts [2h]
- Attended fablab with Tomi and cut the box out from cardboard and assembled it [1h 30 min]
- Swapped broken servos for new ones during the same fablab session.
- Added code for an indicator led which lights up when button has been pushed down long enough [1h]

### Reflection
  
- After uploading rotation code to the Arduino nano, it would seem that also our servos aren't working. Arduino stalls when servo is connected directly to the extension board. Maybe the current draw is too much for the output pin, but using an external power supply doesn't work either. Powering the included dc motor works fine in both cases but our small servo does not. Even though it would be weird that our both servos are broken, I can't see any other reason for this issue.
- External interrupts maybe could be used for the entire project, depending on if multiple interrupt routines can be attached to the same button. This would save some power since state tracking would only occur when the user is interacting with the box. Because we don't have a way to measure our power usage currently, it would be hard to gauge how beneficial the added code complexity would be.
- It would seem that Arduino nano doesn't support deep sleep functionality which it would be woken up from using an interrupt. Also I couldn't figure out an easy way to trigger interrupts while the button was pressed down, I eventually chose to not use interrupts.
- As I suspected both of the servos were broken. Luckily we managed to get new ones pretty fast.
- The cardboard box itself was pretty sturdy but the hinge did not work at all. Hinge joints were too flimsy when cut from cardboard and there were too much friction between the parts. The box was unnecessarely large so some adjustments to the box will be made before we cut the final one from plywood / MDF.

## Tomi Ahonen

- Learned to use the laser cutter at fablab and made a box with hinges from cardboard as a practise so we can make our final box from plywood. The hinges of the cardboardbox were bad as expected.
- For some reason both of our servos were broken and we had to also switch to arduino nano and later we returned back for a new arduino uno. Everyhting is working now.
- Went to a local electronics shop to get a sound sensor for the project. 

## Tommi väisänen

- Troubleshooted arduino uno and learned more about university fabrication capabilities.

## 20-4-2022 Group Tutoring
  
- Timeschedules didn't match available time slots.

# Week 4 [25-4-2022 - 1-5-2022]

## Goals
  
- Design a smaller box 
- Cut new box from MDF or plywood.
- Figure out how to attach components to the box. Need to drill holes for leds and a button. Also cut out a slit for servo arm which will keep the box closed.
- Figure out how components should be connected. Using a breadboard may lead to bad connections, but components wouldn't be reusable if we solder the connections.
- Implement sound sensor into the code.
- Maybe add a led for indicating if enough sound is being produced.
- Solder wires to the button.

## Henrik Peteri

### Tasks
  
- Attended fablab with the group and cut new box from MDF. Also tried reading the sound sensor [1h]
- Updated weekly report [30 min]
- Updated code to read sound sensor and added second indicator led for sound sensor activation [2h]
- Assembled components on the breadboard [1h]

### Reflection
  
- Sound sensor reading wasn't as simple as I was expecting. Potentiometer can be adjusted which determined at which level digital out is high, but we weren't able to get a stable signal from the sensor. 

## Tommi Väisänen

- got more familiar with fablab tools and learned how to use soldering area and 3d printer.

## Group tasks [8h]
  
- Group meetup through discord where a new box was designed.
- Group meetup at fablab and cut new box.
- Group meetup at fablab and cut holes into the box for components.
- Solder wires to the button.
- Attend TA session at 14.30.
- Planned how to fit electronics into the box.
- 3D printed servo holder provided by Georgi.
- Hero shots of the assembled project.

## Group Reflection
  
- New box was generated for 5mm thick material but 3mm material was used. This caused the finger joints to not be flush with the box's sides. Doesn't really affect the functionality and it looks intentional so we won't be cutting a new box.
- Holes maybe should have been designed into the svg file. We weren't sure if the servo would have interfered with opening the box and cardboard prototype didn't have working hinges. We still could have cut holes for the leds and button.
- Since Arduino nanos extension board too much space we used Arduino nano directly placed into the breadboard. This saved alot of space. Battery pack was taped onto a piece of cardboard which was glued into the box. This way we can easily reuse the battery pack. Leds were pressure fitted to the holes but the button was hot glued into place since there were no other easy way to attach it to the boxes lid. Breadboard was attached to the box using doublesided tape. Servo was pressure fitted into the 3d printed servo holder. We managed to make every electronic component reusable except the push button. Hot glue would have to be removed and wires desoldered in order to reuse the button. Altough the wires are directly attached to the breadboard so the button can still be used with the wires attached.

## 27-4-2022 Group Tutoring
  
- Learned how to use 3d printer.
- Got feedback on how components and wires should be managed.

# Week 5 [2-5-2022 - 8-5-2022]

## Henrik Peteri

### Tasks
  
- Updated weekly report [1h]
- Updated final report [2h]

