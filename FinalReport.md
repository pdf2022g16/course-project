# Hero shots

![image](images/hero_open.jpg)

![image](images/hero_closed.jpg)

![image](images/hero_front.jpg)

![image](images/hero_top.jpg)
    
![image](images/hero_indicatorLed.jpg)
  
  
# What have you done

Our original idea was to only make an operational lock by 3D printing a few moving parts for it, which would have been controlled by a servo motor and voice.
During the first presentation, we were introduced to the idea that we could also make a box that can be locked and unlocked by using enough sound.
In the end, we had a laser-cut box with hinges that could be opened by tapping the box hard enough for five seconds while pressing the button on top.
It could be locked again by pressing the button for one second. Inside the box, we had a breadboard, Arduino nano, a power supply, two LEDs, a microphone,
and a button. (see image 1).

# Where did we fail
 
Cardboard prototype wasn't utilized to its full potential. We didn't use it to plan holes for the components, which led to us needing to cut them by hand.
  
![image](images/laatikko2.jpg)
  
Communication and distribution of tasks could have been better. Code was done by one person, but there really wasn't a lot to be done, meaning it would have been difficult to distribute programming tasks to multiple people. We still should have gone through the code as a group to make sure everyone is up to date on functionality and feedback could have been given. Other tasks were done mostly as a group, which would mean we failed to distribute individual tasks to group members. This is mainly due to the project being fairly simple and too compact to be split into smaller sections.
  
We didn't realize that sound sensor wasn't provided with the components even though all of us read the component listing. We also failed to order it when there was a opportunity to do so.
  
We didn't try uploading code to the arduino until a couple of weeks had passed. When we tried to do so the Arduino was broken and new one had to be acquired in bit of a panic. Both of the provided servos were also broken which we managed to get handled really quickly.
  
In the weekly report learning git was provided one of the goals for the group. This goal wasn't reached according to some of our group members.

# What are we missing
   
Originally we had ideas to implement a magnetic sensor which could be added in later implementations but we decided on a simpler button based mechanism which requires the user to manually lock the box after closing. The box has a lot of visible components which could be hidden with a smaller battery and some sort of cover mounted to the lid of the box but since this is a prototype we thought it's fine to leave it as is to allow for easier modification/tuning.

# Where did we succeed

During the course we learned to safely utilize the many tools provided to us in FabLab including 3D printer, laser-cutter, soldering iron and an ultrasonic cutter. In addition we managed to deploy a more simple and robust design than originally planned which also utilizes less material.
We also maanged to quickly recover from the broken arduino and servos and managed to complete our project in time even though our group consists of only 3 members.

We also succeeded in adapting to group projects which is a big plus considering the futures of our careers. Managed to stay on time with the project and keep track of the progress we made. 

# Sustainability

The project is fully reusable and the locking mechanism can be repurposed for other projects if needed. The box can also be repurposed as just a basic box without the locking part to have more storage space. 

# Lessons learned / things we will do differently

We will definitely be more attentive in early stages of production in the future to avoid problems with components like we had with this project. We also learned to distribute workload better among project members to avoid strain on individual memebers of the team. Mostly we learned to make sure to fully utilize the very early stages of the project to create a solid base for our final product by thinking ahead instead of only testing current ideas.

# Feedback

The responsibles were really helpful and aided us in anything we needed help with. The moodle page could be clearer to read. The responsibles were really quick to answer and easy to reach.
